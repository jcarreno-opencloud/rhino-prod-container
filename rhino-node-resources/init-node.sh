#!/bin/bash
set -e

[[ -z ${NODE_ID} ]] && echo "Env variable NODE_ID required" && exit 1

function wait_for_postgres(){
  set +e
  counter=0 
  while [[ $counter -lt ${POSTGRES_MAX_ATTEMPTS:-5} ]] 
  do 
      echo trying...$counter 
      nc -w 1 -zv ${POSTGRES_HOST:-postgres} ${POSTGRES_PORT:-5432} 
      if [[ $? -eq 0 ]]
      then 
          break
      else 
          sleep 1
          counter=$((counter +1))
      fi
  done
  if [[ $counter -eq ${POSTGRES_MAX_ATTEMPTS:-5} ]]
  then 
      echo "Could not verify connectivity to the management db"
      exit 1
  fi
  set -e
}

wait_for_postgres

if [[ -d rhino/node-${NODE_ID} ]]
then
  echo found node dir, skipping node creation
else
    # LOCALIPS is created in a previous stage, in a previous container with different network; we need to override it
    sed -i '/LOCALIPS=/d' /rhino/etc/defaults/config/config_variables
    java -cp resources/rhino-install/rhino-tools.jar com.opencloud.rhino.tools.NetworkQuery >> /rhino/etc/defaults/config/config_variables

    # Enable remote connections so we can run a console in the host
    sed -i 's|//permission java.net.SocketPermission "remotehost", "accept,resolve";|permission java.net.SocketPermission "*", "accept,resolve";|' /rhino/etc/defaults/config/permachine-mlet.conf

    RHINO_DB_NAME=rhino_${CLUSTER_ID:-100}
    sed -i "s/SAVANNA_CLUSTER_ID=.*$/SAVANNA_CLUSTER_ID=${CLUSTER_ID:-100}/" /rhino/etc/defaults/config/config_variables
    sed -i "s/MANAGEMENT_DATABASE_NAME=.*$/MANAGEMENT_DATABASE_NAME=${RHINO_DB_NAME}/" /rhino/etc/defaults/config/config_variables
    sed -i "s/HEAP_SIZE=.*$/HEAP_SIZE=${HEAP_SIZE:-3072m}/" /rhino/etc/defaults/config/config_variables
    
    rhino/create-node.sh ${NODE_ID} ${INIT_MANAGEMENT_DB:-n}  ${PRIMARY:-n}

    if [[ -f resources/options ]]
    then
        cp resources/options rhino/node-${NODE_ID}/config/
    elif [[ ! -z "${STRATEGY_2NODE}" ]]
    then
        echo 'OPTIONS="$OPTIONS -Dcom.opencloud.rhino.component_selection_strategy=2node"' > rhino/node-${NODE_ID}/config/options
    fi
fi


rhino/node-${NODE_ID}/start-rhino.sh -s 

