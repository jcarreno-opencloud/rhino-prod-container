#/bin/bash
set -e

. ./.properties

function build_rhino_prod_image(){
    docker build -t rhino-prod .
}
build_rhino_prod_image