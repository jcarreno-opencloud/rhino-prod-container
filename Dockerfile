FROM centos:7 AS centos-java
LABEL maintainer=juanan

COPY ./jdk/ /opt/java

RUN update-alternatives --install /usr/bin/javac javac /opt/java/bin/javac 100 && \
    update-alternatives --install /usr/bin/java java /opt/java/bin/java 100 && \
    update-alternatives --install /usr/bin/jar jar /opt/java/bin/jar 100

RUN yum update -y \
&& yum -y install  unzip which nc

ENV JAVA_HOME=/opt/java
ENV PATH=${JAVA_HOME}/bin/:${PATH}

FROM centos-java AS centos-java-ant
COPY ./ant/ /opt/ant
ENV ANT_HOME=/opt/ant
ENV PATH=${ANT_HOME}/bin/:${PATH}

#Image in aws repo
#ARG JAVA_VER=11
#FROM 162639413077.dkr.ecr.ap-southeast-2.amazonaws.com/centos-java:java${JAVA_VER}
# built locally with docker-scripts/centos-java
#FROM 162639413077.dkr.ecr.ap-southeast-2.amazonaws.com/centos-java:juanan-11.0.6
#Image built with this dockerfile
FROM centos-java-ant AS rhino-version
LABEL maintainer=juanan

# Move rhino into container
COPY ./rhino-install-resources ./resources
RUN find resources -name "*.sh" -exec chmod +x {} \;
RUN resources/install-rhino.sh

FROM rhino-version
COPY ./rhino-node-resources ./resources
RUN find resources -name "*.sh" -exec chmod +x {} \;
# Set start script
CMD resources/init-node.sh