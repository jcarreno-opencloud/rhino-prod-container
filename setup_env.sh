#/bin/bash
set -e

. ./.properties
function clean(){
    [[ -d "tmp" ]] && rm -r tmp
    [[ -d "client" ]] && rm -r client
    [[ -d "jdk" ]] && rm -rf jdk
    [[ -d "ant" ]] && rm -rf ant
    [[ -f rhino-install-resources/rhino-install.tar ]] && rm  rhino-install-resources/rhino-install.tar
    [[ -d packages ]] && rm -r packages
    :
}
function check_versions(){
    java_output=$(jdk/bin/java -version 2>&1)
    # If there is an error, we need to output it since we redirected stderr
    if [ $? -ne 0 ]
    then
        echo "Could not check java version: $java_output" >&2
        exit 1
    fi
    JAVA_VERSION=$(sed -n -r 's/(java|openjdk) version "([^"]*)".*/\2/p' <<< "${java_output}")
    java_major=${JAVA_VERSION%%.*}
    rhino_major=${RHINO_BRANCH%%.*}

    if [[ $java_major -ge 11 && rhino_major -lt 3 ]]
    then 
        echo "Version mismatch. Java ${JAVA_VERSION} requires Rhino 3 (you are using ${RHINO_BRANCH})"
        exit 1
    elif [[ $java_major -lt 11 && rhino_major -ge 3 ]]
    then
        echo "Version mismatch. Rhino ${RHINO_BRANCH} requires  at least Java 11 (you are using ${JAVA_VERSION})"
    fi

}
function copy_jdk(){
    
    echo "Copying ${JAVA_HOME} to jdk"
    cp -RL "${JAVA_HOME}" jdk/

    files_to_remove=(
        jdk/*src.zip
        jdk/jre/lib/fonts
        jdk/jre/lib/deploy
        jdk/jre/lib/amd64/libjfxwebkit.so
        jdk/jre/lib/ext/jfxrt.jar
        jdk/lib/missioncontrol
        jdk/lib/visualvm
        jdk/man
        )

    for file in "${files_to_remove[@]}"
    do
        :
        echo "Removing ${file}"
        rm -rf "${file}"
    done
}

function copy_ant(){
    echo "Copying ${ANT_HOME} to jdk"
    cp -RL "${ANT_HOME}" ant/
    rm -rf ant/manual
}
function get_rhino(){
    oc-retrieve.sh rhino/${RHINO_BRANCH} rhino latest.release --dest packages 
    rm packages/*unobfuscated* packages/*src*
    cp packages/rhino-install.tar rhino-install-resources/
}

function get_test_license(){
     oc-retrieve.sh -v -d . test-license/trunk test-license latest.integration --dest rhino-install-resources
}


clean
copy_jdk
copy_ant
check_versions
get_test_license
get_rhino

